const each = require('foreach');

//String Array of 0 and 1
var stringArray = ["10100", "10111", "11111", "10010"];

//2D Array of 0 and 1
var _2dArray = [];

//convert StringArray to 2D array
var get2DArray = function(stringArray){
    each(stringArray, function(element, index){
        var arrayData = []
        each(element, function(char, index){
            arrayData.push(char);
        });
        _2dArray.push(arrayData);
    });
};

get2DArray(stringArray);

console.log(_2dArray);

    let startRow = -1;
    let endRow = -1;
    let startColumn = -1;
    let endColumn = -1;
    let maxSize = 0;

//Get Area of Square with all 1's
var getSquareWithAll1 = function(array2D){
    each(array2D, function(element, rowIndex){
        each(element, function(val, colIndex){
            if(val == 1) {
                if(rowIndex+1 <= array2D.length-1 && colIndex+1 <= element.length-1){
                    if(array2D[rowIndex][colIndex+1] == 1 && array2D[rowIndex+1][colIndex] == 1 && array2D[rowIndex+1][colIndex+1] == 1 && maxSize < 2*2){
                        console.log(rowIndex, colIndex);
                        maxSize = 2*2;
                        startRow = rowIndex;
                        startColumn = colIndex;
                        endRow = rowIndex + 1;
                        endColumn = colIndex + 1;
                        if(rowIndex+2 <= array2D.length-1 && colIndex+2 <= element.length-1){
                            if(array2D[rowIndex][colIndex+2] == 1 && array2D[rowIndex+1][colIndex+2] == 1 && array2D[rowIndex+2][colIndex] == 1 && array2D[rowIndex+2][colIndex+1] == 1 && array2D[rowIndex+2][colIndex+2] == 1 && maxSize < 3*3){
                                console.log(rowIndex, colIndex);
                                maxSize = 3*3;
                                endRow = rowIndex+2;
                                endColumn = colIndex+2;
                                if(rowIndex+3 <= array2D.length-1 && colIndex+3 <= element.length-1){
                                    if(array2D[rowIndex][colIndex+3] == 1 && array2D[rowIndex+1][colIndex+3] == 1 && array2D[rowIndex+2][colIndex+3] == 1 && array2D[rowIndex+3][colIndex] == 1 && array2D[rowIndex+3][colIndex+1] == 1 && array2D[rowIndex+3][colIndex+2] == 1 && array2D[rowIndex+3][colIndex+3] == 1 && maxSize < 4*4){
                                        console.log(rowIndex, colIndex);
                                        maxSize = 4*4;
                                        endRow = rowIndex+3;
                                        endColumn = colIndex+3;
                                    } 
                                } 
                            }
                        }
                    } 
                } else {
                    if(maxSize < 1*1) {
                        startRow = endRow = rowIndex;
                        startColumn = endColumn = colIndex;
                        maxSize = 1*1;
                    }
                }
            }
        });
    });
};

getSquareWithAll1(_2dArray);
console.log('startRow: '+startRow);
console.log('startColumn: '+startColumn);
console.log('endRow: '+endRow);
console.log('endColumn: '+endColumn);
console.log('maxSize: '+maxSize);