const fileSystem = require('fs');
const Person = require('./Person');
const each = require('foreach');
const Json2csvParser = require('json2csv').Parser;
const csv2json = require('csvtojson');

let shiva = new Person.Person("Shiva","Dixit","31","Female");
let priyansh = new Person.Person("Priyansh","Dixit","5","Male");
let inwinder = new Person.Person("Inwinder","Kochhar","38","Male");
let puneet = new Person.Person("Puneet","Vyas","35","Male");

// console.log(`${JSON.stringify(shiva)}, Full Name: ${shiva.getName()}, Is Adult: ${shiva.isAdult()}`);
// console.log(`${JSON.stringify(priyansh)}, Full Name: ${priyansh.getName()}, Is Adult: ${priyansh.isAdult()}`);
// console.log(`${JSON.stringify(inwinder)}, Full Name: ${inwinder.getName()}, Is Adult: ${inwinder.isAdult()}`);
// console.log(`${JSON.stringify(puneet)}, Full Name: ${puneet.getName()}, Is Adult: ${puneet.isAdult()}`);

// Creating a data array for Person objects
let data = [shiva, priyansh, inwinder, puneet];

//check if directory doew exists
let dirPath = __dirname + '\\output';
if(!fileSystem.existsSync(dirPath)) {
    //creating a new directory if not exists
    fileSystem.mkdirSync(dirPath, 0o777);
}

//Specify file to be written objects in
let filePath = dirPath + '\\person.json';

//write data array objects into file
var write2file = function(data){
    fileSystem.writeFileSync(filePath, '[');
    each(data, function(element, index){
        fileSystem.appendFileSync(filePath, JSON.stringify(element));
        if(index != data.length-1) {
            fileSystem.appendFileSync(filePath, ',\n');
        }
    });
    fileSystem.appendFileSync(filePath, ']');
};

write2file(data);

//read data from file
var readData = JSON.parse(fileSystem.readFileSync(filePath).toString());
// console.log("Data from file: ");
// console.log(readData);

//write data read from file in a CSV file
var csvFilePath = dirPath + '\\data.csv';

var write2csvFile = function(csvFilePath, data){
    const fields = ['firstName', 'lastName', 'age', 'gender'];

    const json2csvParser = new Json2csvParser({ fields });
    fileSystem.writeFileSync(csvFilePath, json2csvParser.parse(data));
};

write2csvFile(csvFilePath, readData);

//Delete record with specific firstName from file
var deleteRecordFromFile = function(fieldKey, fieldValue, filePath){
    //Convert CSV file to JSON
    console.log('Reading CSV file for processing.')
    var data2write = [];
    csv2json().fromFile(filePath).on('json', (jsonObject)=>{
        console.log(jsonObject);
        if(jsonObject[fieldKey].toLowerCase() != fieldValue.toLowerCase()){
            data2write.push(jsonObject);
        }
    }).on('done', () =>{
        console.log('completed file processing');
        console.log(data2write);
        write2csvFile(csvFilePath, data2write);
    })
};

deleteRecordFromFile('firstName', 'shiva', csvFilePath);