function Person (firstName, lastName, age, gender) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
    this.gender = gender;
};

Person.prototype.getName = function(){
    return `${this.firstName} ${this.lastName}`;
};

Person.prototype.isAdult = function(){
    if(this.age >= 18){
        return true;
    }
    return false;
};

module.exports = {
    Person : Person
}