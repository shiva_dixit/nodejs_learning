var adder = require('./util/adder');
var counter = require('./util/counter');
var multiplier = require('./util/multiplier');

function print() {
    counter.count(['Manish', 'Saraswati', 'Pratibha']);
    adder.add(2, 3);
    multiplier.multiply(2, 3);
};

module.exports = {
    adder: adder,
    counter: counter,
    multiplier: multiplier  
}